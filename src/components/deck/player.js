import React, { useEffect, useState, memo } from 'react';
import PlayerCard from "./playercards";
import Socket from "../../socket/index";
import usePos from "../../custom-hooks/usePos"
import { View, Text, StyleSheet } from 'react-native'
const Player = ({ name }) => {
    // usePos takes player name as argument and return either teammate , first opponent or second opponent so we use these
    // position to place the player in screen
    const pos = usePos(name);
    const [playercard, setPlayercard] = useState(false);
    const [showall, setShowall] = useState(false);
    useEffect(() => {
        Socket.on("cards", function () {
            setPlayercard(!playercard);
        });
        Socket.on("hokm", function () {
            setShowall(!showall);
        });
    }, []);
    return (
        <View style={styles[pos]}>
            <Text>{name}</Text>
        </View>


    );

}
const styles = StyleSheet.create({
    teamate: {
        position: 'absolute',
        transform: [{ translateX: -10 }],
        backgroundColor: "green",
        top: 0,
        zIndex: 100000,
        top: 0,
        left: "50%",

    },
    first: {
        backgroundColor: "red",
        position: 'absolute',
        right: 0,
        top: '50%',

    },
    second: {
        backgroundColor: "yellow",
        position: 'absolute',
        left: 0,
        top: '50%'

    }
})
export default memo(Player);