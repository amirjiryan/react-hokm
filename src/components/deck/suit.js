import React from 'react';
import { Image, StyleSheet, View } from 'react-native'
const images = {
    khaj: require('./../../img/khaj.png'),
    del: require('./../../img/del.png'),
    pik: require('./../../img/pik.png'),
    khesht: require('./../../img/khesht.png')
}
const Suit = ({ suit }) => {
    return (
        <View style={styles.container}>
            <Image
                source={images[suit]}
                style={styles.image}
            />
        </View>
    );
}
const styles = StyleSheet.create({
    image: {
        width: 40,
        height: 46,
        overflow: "visible"
    },
    container: {
        paddingTop: 100,
        marginLeft : 10,
        marginRight:10
    }
});
export default Suit;