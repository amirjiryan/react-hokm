import React from 'react';
import Deck from '../components/deck/deck';
import { useDispatch, useSelector } from 'react-redux';
import Cards from '../components/cards/cards';
import Socket from "../socket/index";
import Infos from '../components/status/infos';
import Players from "../components/deck/players"
import Hokm from "../components/deck/hokm"
import { removeGame } from '../store/actions/game';
import { View, Text, StyleSheet } from 'react-native'
const Game = ({ gameName, name }) => {
    const dispatch = useDispatch();
    // error handling should be added to socket.emit 
    Socket.emit("join-game", gameName, name);
    Socket.on("err", function (error) {
        if (error === "Game is full") {
            dispatch(removeGame())
        }
    });
    return (
        <>
            <Infos game={gameName} />
            <View style={styles.game}>
                <Hokm />
                <Players />
                <Deck />
                <Cards />
            </View>
        </>
    );
}
const styles = StyleSheet.create({
    game: {

        flex: 1,
        alignItems: "center",
        justifyContent: "center",

    }
})
export default Game